#!/usr/bin/env bash

w=3840
h=2160
let "w_=w/2"
x=0

for ((i=1; i<=2; i++))
do


    if ((i == 1)); then
        eval "firefox-esr -p 1 &"
    else
        let "x=w_"
        eval "firefox-esr -p 2 &"
    fi

    echo "i = $i"


    echo "start wait"
    sleep 14   s
    echo "exec"

    id=$(wmctrl -l |grep Mozilla | awk '{print $1}' | tail -1)
    echo $id

    wmctrl -ia $id
    wmctrl -ir $id -b remove,fullscreen
    wmctrl -ir $id -b remove,maximized_vert,maximized_horz
    wmctrl -ir $id -e 0,$x,0,$w_,$h
    wmctrl -ir $id -b add,fullscreen

done