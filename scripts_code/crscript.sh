#!/usr/bin/env bash

# Проверка количества аргументов
if [ $# -ne 2 ];
then
    echo "========="
    echo "Too little arguments"
    echo "========="
    exit 101
fi

# Определение расширения файла скрипта
case $1 in
    "bash") ext=".sh"
    ;;
    "python") ext=".py"
    ;;
    *) exit 102
    ;;
esac

# Создание файла гиперссылки скрипта
scriptname="$2$ext"
scriptpath="/home/oigen/bash_scripts/scripts_code/$scriptname"

eval "touch $scriptpath" 
eval "chmod 775 $scriptpath"
eval "sudo ln -s $scriptpath /usr/local/bin/$2"

echo "$1 script created successfully"
echo "$scriptpath"

# Добавление описания скрипта
echo "Enter script description:"
read description
eval "echo '$scriptname  -  $description' >> /home/oigen/bash_scripts/overview.txt"
